#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import logging

import docker
import boto3

from mhs_ci.utils import get_ecr_docker_client, publish_to_ecr, cleanup_ecr_repo
from mhs_ci.log_config import log_level_names


logger = logging.getLogger()

ROOT = os.path.dirname(os.path.abspath(__file__))
DOCKER_ROOT = os.path.join(ROOT, 'docker')
REPO_PREFIX = 'mhs-ci'


GATEWAY_IMAGE_NAME = 'gateway-software-ci'
LAMBDA_IMAGE_NAME = 'lambdas-ci'
SAGEMAKER_IMAGE_NAME = 'ml-models-ci'
FRONTEND_IMAGE_NAME = 'frontend-ci'
CI_IMAGE_NAME = 'mhs-ci'


DOCKERFILE_MAP = {
    GATEWAY_IMAGE_NAME: os.path.join(DOCKER_ROOT, 'Dockerfile.gateway-software'),
    LAMBDA_IMAGE_NAME: os.path.join(DOCKER_ROOT, 'Dockerfile.lambdas'),
    SAGEMAKER_IMAGE_NAME: os.path.join(DOCKER_ROOT, 'Dockerfile.ml-models'),
    FRONTEND_IMAGE_NAME: os.path.join(DOCKER_ROOT, 'Dockerfile.frontend-ci'),
    CI_IMAGE_NAME: os.path.join(DOCKER_ROOT, 'Dockerfile.mhs-ci'),
}

SELF_UPDATE_MODE = 'self'
CLOUD_UPDATE_MODE = 'cloud'

SELF_UPDATE_IMAGES = (CI_IMAGE_NAME,)
CLOUD_UPDATE_IMAGES = (GATEWAY_IMAGE_NAME, LAMBDA_IMAGE_NAME,
                       SAGEMAKER_IMAGE_NAME, FRONTEND_IMAGE_NAME)

MODE_MAP = {
    SELF_UPDATE_MODE: SELF_UPDATE_IMAGES,
    CLOUD_UPDATE_MODE: CLOUD_UPDATE_IMAGES,
}


def build(images):
    docker_client = docker.from_env()

    for image_name in images:
        dockerfile = DOCKERFILE_MAP[image_name]
        logger.info(f'Building "{image_name}"')
        image, _ = docker_client.images.build(path=ROOT, dockerfile=dockerfile,
                                              tag=image_name, rm=True)


def publish(profile, cleanup, versions, images):
    session = boto3.Session(profile_name=profile)
    ecr_client = session.client('ecr')
    docker_client = get_ecr_docker_client(ecr_client)

    for image_name in images:
        image = docker_client.images.get(image_name)
        repo_name = f'{REPO_PREFIX}/{image_name}'
        publish_to_ecr(image, repo_name, versions, ecr_client, docker_client,
                       cleanup)


def cleanup(profile, images):
    session = boto3.Session(profile_name=profile)
    ecr_client = session.client('ecr')

    for image_name in images:
        repo_name = f'{REPO_PREFIX}/{image_name}'
        cleanup_ecr_repo(ecr_client, repo_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    image_group = parser.add_mutually_exclusive_group()
    image_group.add_argument('--mode', required=False,
                             choices=(SELF_UPDATE_MODE, CLOUD_UPDATE_MODE),
                             help=f'"{SELF_UPDATE_MODE}" to operate with image '
                                  f'for this repo, "{CLOUD_UPDATE_MODE}" to '
                                  f'operate with images for other repos')
    image_group.add_argument('--image', required=False,
                             help='Image name to process')

    subparsers = parser.add_subparsers(dest='func', metavar='command',
                                       help='Command to execute')
    subparsers.required = True

    build_parser = subparsers.add_parser('build')
    build_parser.set_defaults(func=build)

    publish_parser = subparsers.add_parser('publish')
    publish_parser.set_defaults(func=publish)
    publish_parser.add_argument('-p', '--profile', default=None,
                                help='AWS configuration profile')
    publish_parser.add_argument('--no-cleanup', action='store_false',
                                default=True, dest='cleanup',
                                help='Prevent removing untagged images')
    publish_parser.add_argument('--versions', nargs='*',
                                help='Versions tag to add to images')

    cleanup_parser = subparsers.add_parser('cleanup')
    cleanup_parser.set_defaults(func=cleanup)
    cleanup_parser.add_argument('-p', '--profile', default=None,
                                help='AWS configuration profile')

    parser.add_argument('--log_level', default='INFO',
                        choices=log_level_names,
                        help='logging level (default is INFO)')

    args = vars(parser.parse_args())
    log_level = args.pop('log_level')
    func = args.pop('func')

    logging.basicConfig(
        format='[%(levelname)s] %(asctime)s: %(message)s',
        level=log_level
    )

    mode = args.pop('mode')
    image = args.pop('image')
    if mode:
        images = MODE_MAP[mode]
    else:
        images = (image, )
    args['images'] = images

    func(**args)
