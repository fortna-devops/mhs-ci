# MHS CI

This repo provides interface to deploy lambdas, gateways and ML models on AWS

## Usage
### Processing lambdas

#### Syntax of `.config.ini`

`.config.ini` is a general configuration file for lambdas and used for both steps (test and deploy)

```ini
[basic]
name=some-lambda-name
runtime=python3.6
handler=some-lambda-name.handler
role=arn:aws:iam::000000000000:role/service-role/some-lambda-role
; "memory" parameter is optional. Default value is 128
memory=256
; "timeout" parameter is optional. Default value is 3
timeout=60

; "env" section is optional
[env]
ENV_KEY=ENV_VALUE

; "tags" section is optional
[tags]
tag_key=tag_value
```

#### Run tests
```
usage: main.py [-h] [--log_level {NOTSET,DEBUG,INFO,WARNING,ERROR,CRITICAL}]
               {lambdas,sagemaker} ...

positional arguments:
  {lambdas,sagemaker}   Module to use

optional arguments:
  -h, --help            show this help message and exit
  --log_level {NOTSET,DEBUG,INFO,WARNING,ERROR,CRITICAL}
                        logging level (default is INFO)
```

To run tests automatically on bitbucket lambda function should meet next conditions:

For python based lambdas:
* Have `tox.ini` file
* Tests should be in `test*.py` files and this files should be importable from root dir. If you want to place tests in some dir (e.g. tests) this dir should be python module (contain `__init__.py`)

For nodeJs based lambdas:
* tests should be placed in `tests` dir.


`tox.ini` example
```
[tox]
envlist = py36  # version of python envioriment
skipsdist = True  # required to skip sdist buid step

[testenv]
deps =
    -r{toxinidir}/requirements.txt  # requirements from file to be installed to test envioriment

commands =
    python -c "import unittest; import xmlrunner; unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'), module=None)"
```

For python unit tests we should use `unittest-xml-reporting` to generate xml-like reports to display test results properly on bitbucket pipeline interface

Example with code coverage support
```
[tox]
envlist = py36
skipsdist = True

[testenv]
deps =
    -r{toxinidir}/requirements-no-deps.txt

whitelist_externals = coverage

commands =
    coverage erase
    coverage run --source=./ --omit="test*",".tox" ../../mhs-ci/unit_test.py discover

```

#### Deploy
```
usage: main.py lambdas deploy [-h] [-p PROFILE] --bucket_name BUCKET_NAME
                              [--alias_description ALIAS_DESCRIPTION]
                              [-s SRC_DIR] --prefix PREFIX
                              version_alias

positional arguments:
  version_alias         alias name that will be assigned to new version

optional arguments:
  -h, --help            show this help message and exit
  -p PROFILE, --profile PROFILE
                        AWS configuration profile
  --bucket_name BUCKET_NAME
                        AWS bucket name
  --alias_description ALIAS_DESCRIPTION
                        alias description (default is "")
  -s SRC_DIR, --src_dir SRC_DIR
                        path to lambdas source dir (default is "./lambdas")
  --prefix PREFIX       prefix for version cache file.
```

##### Dependency resolver
Deployment script looks for dependency file based on `runtime`

* requirements.txt and requirements-no-deps.txt for `python`
* package.json for `nodejs`

Dependencies will be installed and added to lambda archive.

#### Main steps
* Iterate over dirs in `lambdas`
* Parse `.config.ini`
* Parse `.version`
* Check that lambda exists on AWS
* Make an archive with lambda code
* Create or update lambda on AWS and publish new version
* Point alias to published version

#### Additional information
* `tests` dir in lambda root will be ignored and not be included to archive.


## Docker images
There are two mods for images:
* `cloud` to build images for lambdas, gateway and SageMaker
* `self` to build image for this repository

To build new version of images for `cloud`
```
python manage_docker.py --mode=cloud build
```

To publish new version to ECR repositories
```
python manage_docker.py --mode=cloud publish
```

To remove old images from ECR repositories
```
python manage_docker.py --mode=cloud cleanup
```

To update image for this repository you need to change `cloud` to `self`
