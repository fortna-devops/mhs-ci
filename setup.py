from setuptools import setup, find_packages


def get_required():
    with open('requirements.txt') as f:
        required = f.readlines()
    return required


setup(
    name='mhs-ci',
    version='0.0.1',
    description='Continuous integration module for MHS project',
    author='MHS Insights Team',
    author_email='igor.panteleyev@dataart.com',
    packages=find_packages(),
    install_requires=get_required(),
    entry_points={
        'console_scripts': [
            'ci-deploy=mhs_ci.main:main',
        ],
    },
)
