import base64
import logging

import docker

logger = logging.getLogger()


__all__ = ['get_ecr_docker_client', 'get_ecr_auth',
           'get_or_create_ecr_repository', 'publish_to_ecr', 'cleanup_ecr_repo']


def get_ecr_auth(ecr_client):
    auth_data = ecr_client.get_authorization_token()['authorizationData'][0]

    token = auth_data['authorizationToken']
    username, password = base64.b64decode(token).decode("utf-8").split(":")
    registry = auth_data['proxyEndpoint']

    return {
        'username': username,
        'password': password,
        'registry': registry
    }


def get_ecr_docker_client(ecr_client, docker_client=None):
    if docker_client is None:
        docker_client = docker.from_env()

    ecr_auth = get_ecr_auth(ecr_client)
    docker_client.login(**ecr_auth)
    return docker_client


def get_or_create_ecr_repository(ecr_client, repo_name):
    response = ecr_client.describe_repositories()
    for repo in response['repositories']:
        if repo['repositoryName'] == repo_name:
            repo_uri = repo['repositoryUri']
            break
    else:
        response = ecr_client.create_repository(repositoryName=repo_name)
        repo_uri = response['repository']['repositoryUri']

    return repo_uri


def publish_to_ecr(image, repo_name, versions, ecr_client, docker_client,
                   cleanup=True):
    logger.info(f'Publishing "{repo_name}"')

    if not isinstance(versions, (list, tuple)):
        versions = [versions]

    repo_uri = get_or_create_ecr_repository(ecr_client, repo_name)

    for version in versions:
        tag = '{}:{}'.format(repo_uri, version)
        image.tag(tag)
        docker_client.images.push(repo_uri)

    if cleanup:
        cleanup_ecr_repo(ecr_client, repo_name)


def cleanup_ecr_repo(ecr_client, repo_name):
    logger.info(f'Removing outdated images from "{repo_name}"')
    images = ecr_client.list_images(
        repositoryName=repo_name,
        filter={
            'tagStatus': 'UNTAGGED'
        }
    )

    image_ids = images['imageIds']

    if not image_ids:
        logger.info('No images to remove')
        return

    ecr_client.batch_delete_image(
        repositoryName=repo_name,
        imageIds=image_ids
    )
