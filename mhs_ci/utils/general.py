import os
import logging
from contextlib import contextmanager


__all__ = ['cd', 'get_code_version']


logger = logging.getLogger()


VERSION_FILE_NAME = '.version'


@contextmanager
def cd(path):
    new_path = os.path.expanduser(path)
    old_path = os.getcwd()
    os.chdir(new_path)
    yield
    os.chdir(old_path)


def get_code_version(path):
    path = os.path.join(path, VERSION_FILE_NAME)
    if not os.path.isfile(path):
        logger.warning(f'Can\'t find "{path}"')
        return ''

    with open(path, 'r') as f:
        return f.readline().strip()
