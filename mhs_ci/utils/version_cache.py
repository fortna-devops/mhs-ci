import os
import pickle


__all__ = ['Versions']


class Versions(object):

    VERSIONS_FILE = 'versions.pickle'

    def __init__(self, bucket_name, prefix, s3fs):
        self._fs = s3fs
        self._s3_path = os.path.join(bucket_name, prefix, self.VERSIONS_FILE)
        self._cache = {}
        self.load()

    def get_cache(self):
        return self._cache

    def get(self, name, alias):
        if name not in self._cache:
            return None

        return self._cache[name].get(alias)

    def set(self, name, alias, value):
        if name not in self._cache:
            self._cache[name] = {}

        self._cache[name][alias] = value

    def load(self):
        if not self._fs.exists(self._s3_path):
            return

        with self._fs.open(self._s3_path) as f:
            self._cache = pickle.load(f)

    def save(self):
        with self._fs.open(self._s3_path, 'wb') as f:
            pickle.dump(self._cache, f)
