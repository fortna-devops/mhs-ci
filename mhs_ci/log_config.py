import logging

__all__ = ['log_level_names']


# noinspection PyProtectedMember
def _get_logging_level_names():
    # find logging's internal dictionary for level names.
    # the internal dict name changed in python 3.4.
    try:
        levels = logging._levelToName
        level_vals = levels.keys()
    except AttributeError:
        levels = logging._levelNames
        level_vals = [key for key in levels.keys() if isinstance(key, int)]

    return [levels[val] for val in sorted(level_vals)]


log_level_names = _get_logging_level_names()
