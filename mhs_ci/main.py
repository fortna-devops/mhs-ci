#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging

from mhs_ci.modules import lambdas, sagemaker, frontend
from mhs_ci.log_config import log_level_names


MODULES_MAP = {
    'lambdas': lambdas,
    'sagemaker': sagemaker,
    'frontend': frontend,
}


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='module', help='Module to use')
    subparsers.required = True

    for module_name, module in MODULES_MAP.items():
        group = subparsers.add_parser(module_name)
        subsubparsers = group.add_subparsers(dest='func', metavar='command',
                                             help='Command to execute')
        subsubparsers.required = True
        for name, command in module.COMMANDS.items():
            command_group = subsubparsers.add_parser(name)
            command_group.set_defaults(func=command.run)
            command.init_args(command_group)

    parser.add_argument('--log_level', default='INFO',
                        choices=log_level_names,
                        help='logging level (default is INFO)')
    args = parser.parse_args()

    args = vars(args)
    del args['module']
    log_level = args.pop('log_level')
    func = args.pop('func')

    logging.basicConfig(
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s',
        level=log_level
    )

    func(**args)


if __name__ == '__main__':
    main()
