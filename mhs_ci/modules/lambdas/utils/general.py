import io
import logging
import os
import zipfile

from mhs_ci.utils import cd


__all__ = ['make_archive']


logger = logging.getLogger()

ARCHIVE_EXCLUDE = ('tests',)


def _skip_path(root, name, rel):
    return root[len(rel):].count(os.sep) == 0 and name in ARCHIVE_EXCLUDE


def make_archive(path, rel='.'):
    logger.info(f'Making archive for "{path}"')
    with cd(path), io.BytesIO() as f:
        with zipfile.ZipFile(f, 'w', compression=zipfile.ZIP_DEFLATED) as zf:
            for dirpath, dirnames, filenames in os.walk(rel):
                for name in sorted(dirnames):
                    path = os.path.normpath(os.path.join(dirpath, name))
                    if _skip_path(dirpath, name, rel):
                        logger.info(f'Skipping "{path}"')
                        dirnames.remove(name)
                        continue
                    zf.write(path, path)
                    logger.debug(f'Adding "{path}" to archive')
                for name in filenames:
                    path = os.path.normpath(os.path.join(dirpath, name))
                    if _skip_path(dirpath, name, rel):
                        logger.info(f'Skipping "{path}"')
                        continue
                    if os.path.isfile(path):
                        zf.write(path, path)
                        logger.debug(f'Adding "{path}" to archive')

        logger.info('Archive created')
        return f.getvalue()
