from __future__ import absolute_import

import sys
import argparse
import copy

import pip._internal.req.req_set as req_set
from pip._internal.main import main as _main


orig_add_req = copy.deepcopy(req_set.RequirementSet.add_requirement)


def add_req(self, install_req, parent_req_name=None, extras_requested=None):
    if install_req.name in exclude:
        print('{} is excluded'.format(install_req.name))
        return [], None

    return orig_add_req(self, install_req, parent_req_name, extras_requested)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--exclude_deps', default='', help='coma-separated list of packages')
    parsed, other = parser.parse_known_args()

    exclude = vars(parsed)['exclude_deps'].split(',')

    req_set.RequirementSet.add_requirement = add_req
    sys.exit(_main(other))
