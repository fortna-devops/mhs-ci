import os
import re
import warnings
from abc import ABC, abstractmethod

from .base import BaseDependency


__all__ = ['Py2Dependency', 'Py3Dependency']


PIP_SCRIPT = 'pip_patch.py'
PIP_LOCATION = os.path.join(os.path.dirname(os.path.abspath(__file__)), PIP_SCRIPT)


class BasePythonDependency(BaseDependency, ABC):
    packages_files = ('requirements.txt', 'requirements-no-deps.txt')

    def __init__(self, base_dir, excluded_dependencies):
        super(BasePythonDependency, self).__init__(base_dir)
        self._excluded_dependencies = excluded_dependencies

    @property
    @abstractmethod
    def pip_executable(self):
        return ''

    def get_executable(self, packages_file):
        executable = self.pip_executable.split()
        executable.extend([
            'install',
            '-r', packages_file,
            '-t', os.curdir
        ])

        if self._excluded_dependencies:
            executable.extend(['--exclude_deps', ','.join(self._excluded_dependencies)])

        if 'no-deps' in packages_file:
            warnings.warn('no-deps is not required any more to skip boto3 '
                          'installation.', DeprecationWarning)
            executable.append('--no-deps')

        return executable


class Py2Dependency(BasePythonDependency):
    pip_executable = 'python2 {}'.format(PIP_LOCATION)
    runtime_pattern = re.compile(r'^python2.7$')


class Py3Dependency(BasePythonDependency):
    pip_executable = 'python3 {}'.format(PIP_LOCATION)
    runtime_pattern = re.compile(r'^python3.[6|7]$')
