import re
import logging

from .python_resolver import Py2Dependency, Py3Dependency
from .node_resolver import NodeJSDependency


__all__ = ['get_dependency']


logger = logging.getLogger()


KNOWN_DEPENDENCY = [Py2Dependency, Py3Dependency, NodeJSDependency]


def get_dependency(runtime):
    for depend in KNOWN_DEPENDENCY:
        if re.match(depend.runtime_pattern, runtime):
            return depend
    else:
        logger.warning(f'No resolver found for "{runtime}"')
