import os
import logging
import subprocess

from abc import ABC, abstractmethod

from mhs_ci.utils import cd


__all__ = ['BaseDependency']


logger = logging.getLogger()


class BaseDependency(ABC):

    @property
    @abstractmethod
    def runtime_pattern(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def packages_files(self):
        raise NotImplementedError

    @abstractmethod
    def get_executable(self, packages_file):
        raise NotImplementedError

    def __init__(self, base_dir, *args, **kwargs):
        self.base_dir = base_dir

    def install(self):
        with cd(self.base_dir):
            for packages_file in self.packages_files:
                if not os.path.isfile(packages_file):
                    logger.info(f'Can\'t found "{packages_file}"')
                    continue

                self.install_from_file(packages_file)

    def install_from_file(self, packages_file):
        cmd = self.get_executable(packages_file)
        logger.info('Executing: {}'.format(' '.join(cmd)))
        subprocess.check_call(cmd)
