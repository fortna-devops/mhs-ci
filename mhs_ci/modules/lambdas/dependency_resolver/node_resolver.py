import re

from .base import BaseDependency


__all__ = ['NodeJSDependency']


class NodeJSDependency(BaseDependency):
    packages_files = ('package.json',)
    runtime_pattern = re.compile(r'^nodejs(\d+.(?:\d+|x))$')

    def get_executable(self, packages_file):
        return ['npm', 'install', '--production']
