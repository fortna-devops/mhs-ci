import logging
import os
import time

from mhs_ci.utils import get_code_version
from .utils.general import make_archive
from .dependency_resolver import get_dependency
from .config import get_config, ENV_SECTION, TAGS_SECTION, VPC_SECTION, EXTRAS_SECTION


logger = logging.getLogger()


class Function(object):

    _is_lambda_exists = None
    _is_alias_exists = None
    _is_update_needed = None

    _update_function_timeout = 120  # seconds
    _update_function_delay = 1  # seconds

    def __init__(self, client, s3fs, version_cache, path, bucket_name, s3_prefix, alias_name,
                 version_description):
        self._client = client
        self._s3fs = s3fs
        self._version_cache = version_cache
        self._path = path
        self._bucket_name = bucket_name
        self._s3_prefix = s3_prefix
        self._alias_name = alias_name
        self._version_description = version_description.strip()[:255]

        self._config = get_config(self._path, self._alias_name)
        self._code_version = get_code_version(self._path)

        self._check_states()

    @property
    def name(self):
        return self._config['name']

    def _check_states(self):
        # check if lambda exists
        try:
            self._client.get_function(FunctionName=self.name)
        except self._client.exceptions.ResourceNotFoundException:
            logger.info(f'Lambda with name "{self.name}" not found, will be created')
            self._is_lambda_exists = False
            self._is_alias_exists = False
            return
        else:
            self._is_lambda_exists = True

        # check if alias exists
        try:
            self._client.get_function_configuration(
                FunctionName=self.name,
                Qualifier=self._alias_name
            )
        except self._client.exceptions.ResourceNotFoundException:
            logger.info(f'No "{self._alias_name}" alias found for lambda "{self.name}", '
                        f'will be created')
            self._is_alias_exists = False
            return
        else:
            self._is_alias_exists = True

        # check if we need to update this version
        version = self._version_cache.get(self.name, self._alias_name)
        if not self._code_version or not version or version != self._code_version:
            self._is_update_needed = True

    @property
    def is_lambda_exists(self):
        return self._is_lambda_exists

    @property
    def is_alias_exists(self):
        return self._is_alias_exists

    @property
    def is_update_needed(self):
        return self._is_update_needed

    def deploy(self):
        if not self.is_lambda_exists:
            logger.info(f'Creating new lambda "{self.name}"')
            self._create_function()
        elif not self.is_alias_exists or self.is_update_needed:
            logger.info(f'Updating existing lambda "{self.name}"')
            self._update_function()
        else:
            logger.info(f'No actions needed for lambda "{self.name}"')

    def _prepare(self):
        # install dependency
        dependency_class = get_dependency(self._config['runtime'])
        excluded_dependencies = self._config[EXTRAS_SECTION]['excluded_dependencies']
        if dependency_class:
            depend = dependency_class(self._path, excluded_dependencies)
            depend.install()

        # set code version
        self._version_cache.set(self.name, self._alias_name, self._code_version)

    def _create_artifact(self):
        suffix = '.zip'
        artifact_name = os.path.join(self._s3_prefix, self.name + suffix)
        path = os.path.join(self._bucket_name, artifact_name)
        with self._s3fs.open(path, 'wb') as f:
            f.write(make_archive(self._path))

        return artifact_name

    def _create_function(self):
        self._prepare()
        artifact_name = self._create_artifact()

        func = self._client.create_function(
            FunctionName=self._config['name'],
            Runtime=self._config['runtime'],
            Role=self._config['role'],
            Handler=self._config['handler'],
            Code={
                'S3Bucket': self._bucket_name,
                'S3Key': artifact_name
            },
            Description=self._version_description,
            Timeout=self._config['timeout'],
            MemorySize=self._config['memory'],
            Environment={'Variables': self._config[ENV_SECTION]},
            VpcConfig={
                'SubnetIds': self._config[VPC_SECTION]['subnet_ids'],
                'SecurityGroupIds': self._config[VPC_SECTION]['security_group_ids']
            },
            Tags=self._config[TAGS_SECTION],
            Publish=True
        )

        self._client.create_alias(FunctionName=self.name, Name=self._alias_name,
                                  FunctionVersion=func['Version'])

    def _update_function(self):
        self._prepare()
        artifact_name = self._create_artifact()

        func_config = self._client.update_function_configuration(
            FunctionName=self._config['name'],
            Runtime=self._config['runtime'],
            Role=self._config['role'],
            Handler=self._config['handler'],
            Description=self._version_description,
            Timeout=self._config['timeout'],
            MemorySize=self._config['memory'],
            Environment={'Variables': self._config[ENV_SECTION]},
            VpcConfig={
                'SubnetIds': self._config[VPC_SECTION]['subnet_ids'],
                'SecurityGroupIds': self._config[VPC_SECTION]['security_group_ids']
            },
        )

        start_poll_time = time.time()
        while True:
            if time.time() - start_poll_time >= self._update_function_timeout:
                raise Exception('Takes too long to update function configuration')

            func_config_status = self._client.get_function_configuration(
                FunctionName=self._config['name']
            )
            func_status = func_config_status['LastUpdateStatus']

            if func_status == 'Successful':
                break

            if func_status == 'InProgress':
                time.sleep(self._update_function_delay)
                continue

            if func_status == 'Failed':
                raise Exception(f'Function update failed. Reason: {func_config_status["LastUpdateStatusReason"]}')

            raise Exception(f'Unknown function status: {func_status}')

        if self._config[TAGS_SECTION]:
            self._client.tag_resource(
                Resource=func_config['FunctionArn'],
                Tags=self._config[TAGS_SECTION],
            )

        func = self._client.update_function_code(
            FunctionName=self._config['name'],
            S3Bucket=self._bucket_name,
            S3Key=artifact_name,
            Publish=True
        )

        if self.is_alias_exists:
            alias_action = self._client.update_alias
        else:
            alias_action = self._client.create_alias

        alias_action(FunctionName=self.name, Name=self._alias_name,
                     FunctionVersion=func['Version'])
