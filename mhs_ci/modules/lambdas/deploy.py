import os
import logging
from argparse import ArgumentParser
from typing import Optional, List

import boto3
from s3fs import S3FileSystem

from mhs_ci.utils import cd, Versions
from .function import Function


logger = logging.getLogger()


__all__ = ['DeployLambdas']


class DeployLambdas:
    
    @staticmethod
    def init_args(parser: ArgumentParser) -> None:
        parser.add_argument('-p', '--profile', default=None, help='AWS configuration profile')
        parser.add_argument('--bucket_name', required=True, help='AWS bucket name')
        parser.add_argument('--alias_description', default='',
                            help='alias description (default is "")')
        parser.add_argument('-s', '--src_dir', default='./lambdas',
                            help='path to lambdas source dir (default is "./lambdas")')
        parser.add_argument('--prefix', required=True, help='prefix for version cache file.')
        parser.add_argument('-f', '--function', nargs='*', dest='func_list',
                            help='deploy only specified function(s). filter by dir name')
        parser.add_argument('version_alias', help='alias name that will be assigned to new version')
    
    @classmethod
    def run(cls, version_alias: str, bucket_name: str, src_dir: str, prefix: str,
            alias_description: Optional[str] = None, profile: Optional[str] = None,
            func_list: Optional[List[str]] = None) -> None:

        session = boto3.Session(profile_name=profile)
        client = session.client('lambda')
        s3fs = S3FileSystem(session=session)

        version_cache = Versions(bucket_name, prefix, s3fs)
        with cd(src_dir):
            for path in os.listdir(os.getcwd()):
                if func_list and path not in func_list:
                    continue
                if not os.path.isdir(path):
                    logger.info(f'Skipping "{path}". It\'s not a dir.')
                    continue

                func = Function(client, s3fs, version_cache, path, bucket_name, prefix,
                                version_alias, alias_description)
                func.deploy()
                version_cache.save()
                logger.info('Done')
