import os
import json
from copy import deepcopy

from .parser import ConfigParser

CONFIG_FILE_NAME = '.config.ini'

BASIC_SECTION = 'basic'
ENV_SECTION = 'env'
TAGS_SECTION = 'tags'
VPC_SECTION = 'vpc'
EXTRAS_SECTION = 'extras'


__all__ = ['get_config', 'BASIC_SECTION', 'ENV_SECTION', 'TAGS_SECTION', 'VPC_SECTION',
           'EXTRAS_SECTION']


def get_config(path, qualifier=None):
    path = os.path.join(path, CONFIG_FILE_NAME)
    if not os.path.isfile(path):
        raise FileNotFoundError(f'Can\'t find "{path}"')

    config_parser = ConfigParser()

    basic_section = config_parser.add_section(BASIC_SECTION)
    basic_section.add_argument('name', required=True)
    basic_section.add_argument('runtime', required=True)
    basic_section.add_argument('handler', required=True)
    basic_section.add_argument('role', required=True)
    basic_section.add_argument('memory', type=int, default=128)
    basic_section.add_argument('timeout', type=int, default=3)

    config_parser.add_section(ENV_SECTION, only_known_args=False, merge=True)
    config_parser.add_section(TAGS_SECTION, only_known_args=False)

    vpcs = config_parser.add_section(VPC_SECTION)
    vpcs.add_argument('subnet_ids', type=json.loads, default=[])
    vpcs.add_argument('security_group_ids', type=json.loads, default=[])

    extras = config_parser.add_section(EXTRAS_SECTION)
    extras.add_argument('excluded_dependencies', type=json.loads, default=[])

    args = config_parser.parse_config(path, qualifier)

    config = deepcopy(args[BASIC_SECTION])
    config[ENV_SECTION] = args[ENV_SECTION]
    config[TAGS_SECTION] = args[TAGS_SECTION]
    config[VPC_SECTION] = args[VPC_SECTION]
    config[EXTRAS_SECTION] = args[EXTRAS_SECTION]

    return config
