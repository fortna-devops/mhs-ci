from configparser import ConfigParser as DefaultConfigParser, _UNSET


__all__ = ['ConfigParser', 'Section', 'Argument']


class IniConfigParser(DefaultConfigParser):

    def optionxform(self, optionstr):
        return optionstr


# stub for default value
_NONE = object()


class Argument(object):

    def __init__(self, name, type=str, default=_NONE, required=False):
        self.name = name
        self.type = type
        self.default = default
        self.required = required

    @property
    def fallback(self):
        if self.required and self.default == _NONE:
            fallback = _UNSET
        else:
            fallback = self.default

        return fallback


class Section(object):

    def __init__(self, name, only_known_args=True, merge=False):
        self.name = name
        self.only_known_args = only_known_args
        self.merge = merge

        self._arguments = []

    def add_argument(self, name, type=str, default=_NONE, required=False):
        self._arguments.append(Argument(name=name, type=type, default=default, required=required))

    def process(self, parser, qualifier=None):
        result = self._process(parser, self.name)

        if self.merge and qualifier is not None:
            key = f'{self.name}:{qualifier}'
            if key in parser.sections():
                result.update(self._process(parser, key))

        return result

    def _process(self, parser, section_key):
        if self.only_known_args:
            result = {}
            for arg in self._arguments:
                value = parser._get_conv(section_key, arg.name, arg.type, fallback=arg.fallback)
                if value != _NONE:
                    result[arg.name] = value
        else:
            result = dict(parser[section_key]) if section_key in parser else {}

        return result


class ConfigParser(object):

    def __init__(self):
        self._parser = IniConfigParser()
        self._sections = []

    def parse_config(self, path, qualifier=None):
        result = {}
        self._parser.read(path)
        for section in self._sections:
            result[section.name] = section.process(self._parser, qualifier)
        return result

    def add_section(self, name, only_known_args=True, merge=False):
        section = Section(name=name, only_known_args=only_known_args,
                          merge=merge)
        self._sections.append(section)
        return section
