import os
import logging
from argparse import ArgumentParser

from mhs_ci.utils import cd
from .config import get_config
from .test_runners import PythonTestRunner, NodeTestRunner, FAILED_STATUSES

logger = logging.getLogger()


__all__ = ['TestLambdas']


TEST_RUNNER_MAP = {
    'python': PythonTestRunner,
    'node': NodeTestRunner,
}


class TestLambdas:
    ARTIFACTS_DIR = 'build_artifacts'
    S3_BASE_URL = 'https://s3.console.aws.amazon.com/s3/buckets/'

    @staticmethod
    def init_args(parser: ArgumentParser) -> None:
        parser.add_argument('-s', '--src_dir', default='./lambdas',
                            help='path to lambdas source dir '
                                 '(default is "./lambdas")')

    @classmethod
    def run(cls, src_dir: str) -> None:
        is_success = cls.test(src_dir)

        logger.info('Done')
        if not is_success:
            exit(1)

    @classmethod
    def test(cls, src_dir: str) -> bool:
        with cd(src_dir):
            success = True
            for path in os.listdir(os.getcwd()):

                if not os.path.isdir(path):
                    logger.info(f'Skipping "{path}". It\'s not a dir.')
                    continue

                config = get_config(path)

                for runtime, runner_cls in TEST_RUNNER_MAP.items():
                    if runtime in config['runtime']:
                        test_runner = runner_cls(path)
                        break
                else:
                    logger.warning(f'Unsupported runtime "{config["runtime"]}"')
                    continue

                with cd(path):
                    test_runner.run()
                    if test_runner.status in FAILED_STATUSES:
                        success = False

            return success
