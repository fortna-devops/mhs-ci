import logging
from abc import ABC, abstractmethod


logger = logging.getLogger()


__all__ = ['BaseTestRunner']


UNKNOWN = 'unknown'
SUCCESS = 'success'
FAIL = 'fail'
SKIP = 'skip'

FAILED_STATUSES = (FAIL,)


class BaseTestRunner(ABC):

    def __init__(self, name):
        self._name = name
        self.__status = UNKNOWN

    @property
    def status(self):
        return self.__status

    def _set_status(self, status):
        self.__status = status

    def success(self):
        self._set_status(SUCCESS)

    def fail(self):
        self._set_status(FAIL)

    def skip(self):
        self._set_status(SKIP)

    def run(self):
        self._run()

    @abstractmethod
    def _run(self):
        return None
