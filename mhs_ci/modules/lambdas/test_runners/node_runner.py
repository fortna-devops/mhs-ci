import os
import logging
import subprocess

from .base import BaseTestRunner


logger = logging.getLogger()


__all__ = ['NodeTestRunner']


class NodeTestRunner(BaseTestRunner):

    def _run(self):
        if not os.path.exists('tests'):
            logger.info(f'No tests in "{self._name}"')
            self.skip()
            return

        logger.info(f'Run tests for "{self._name}"')
        subprocess.call(['npm', 'install'])
        retcode = subprocess.call([
            'nyc',
            '--exclude', 'tests/**',
            '--reporter', 'cobertura',
            'mocha', 'tests/*.js',
            '--reporter', 'mocha-junit-reporter',
            '--reporter-options', 'mochaFile=./test-reports/junit.xml'
        ])
        if retcode != 0:
            self.fail()
