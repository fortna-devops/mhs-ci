import os
import logging
import subprocess

from .base import BaseTestRunner


logger = logging.getLogger()


__all__ = ['PythonTestRunner']


class PythonTestRunner(BaseTestRunner):

    def _run(self):
        if not os.path.exists('tox.ini'):
            logger.info(f'No "tox.ini" in "{self._name}"')
            self.skip()
            return

        logger.info(f'Run tests for "{self._name}"')
        retcode = subprocess.call('tox')
        if retcode != 0:
            self.fail()
