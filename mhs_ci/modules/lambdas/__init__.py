from .deploy import DeployLambdas
from .test import TestLambdas

COMMANDS = {
    'test': TestLambdas,
    'deploy': DeployLambdas
}
