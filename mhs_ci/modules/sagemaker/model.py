import os
import logging

from mhs_ci.utils import cd, publish_to_ecr

__all__ = ['Model']


logger = logging.getLogger()


class Model(object):
    repository_prefix = 'sagemaker'
    latest_version = 'latest'

    def __init__(self, name, ecr_client, docker_client, version_alias,
                 build_args_path):
        self._name = name
        self._repo_name = '{}/{}'.format(self.repository_prefix, self._name)
        self._ecr_client = ecr_client
        self._docker_client = docker_client
        self._version_alias = version_alias
        self._versions = (self.latest_version, self._version_alias)
        self._build_args_path = build_args_path

    def get_build_args(self):
        args = {}
        for root, dirs, files in os.walk(self._build_args_path):
            rel_path = os.path.relpath(root, self._build_args_path)
            if not files:
                continue

            for file in files:
                file_path = os.path.join(root, file)
                arg_name = '_'.join((rel_path, file))

                with open(file_path) as f:
                    args[arg_name] = f.read()

        return args

    def deploy(self):
        with cd(os.path.join(self._name)):
            logger.info(f'Building docker image for "{self._name}"')
            build_args = self.get_build_args()
            image, _ = self._docker_client.images.build(
                path='.', rm=True, buildargs=build_args)

        publish_to_ecr(image, self._repo_name, self._versions, self._ecr_client,
                       self._docker_client)
