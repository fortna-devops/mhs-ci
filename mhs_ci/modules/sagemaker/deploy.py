import os
import logging
from argparse import ArgumentParser
from typing import Optional

import boto3
import docker

from mhs_ci.utils import cd, get_ecr_auth
from .model import Model


logger = logging.getLogger()


__all__ = ['DeployModels']


class DeployModels:

    @staticmethod
    def init_args(parser: ArgumentParser) -> None:
        parser.add_argument('-p', '--profile', default=None,
                            help='AWS configuration profile')
        parser.add_argument('-s', '--src_dir', default='./models',
                            help='path to models source dir '
                                 '(default is "./models")')
        parser.add_argument('-a', '--build_args_dir', default='./build_args',
                            help='path to build args for docker images '
                                 '(default is "./build_args")')
        parser.add_argument('version_alias',
                            help='alias name that will be assigned to new '
                                 'version')

    @classmethod
    def run(cls, src_dir: str, build_args_dir: str, version_alias: str,
            profile: Optional[str]=None) -> None:

        docker_client = docker.from_env()

        session = boto3.Session(profile_name=profile)
        ecr_client = session.client('ecr')

        ecr = get_ecr_auth(ecr_client)
        docker_client.login(**ecr)

        build_args_path = os.path.abspath(build_args_dir)

        with cd(src_dir):
            for path in os.listdir(os.getcwd()):

                if not os.path.isdir(path):
                    logger.info(f'Skipping "{path}". It\'s not a dir.')
                    continue

                model = Model(path, ecr_client, docker_client, version_alias,
                              build_args_path)
                model.deploy()
                logger.info('Done')
