from .deploy import DeployFrontend, DeployFrontendLogin

COMMANDS = {
    'deploy': DeployFrontend,
    'deploy_login': DeployFrontendLogin
}
