import argparse
import json
import subprocess
import io
import time
from typing import Optional

import boto3


__all__ = ['DeployFrontend', 'DeployFrontendLogin']


class BaseDeployFrontend:

    @staticmethod
    def init_args(parser: argparse.ArgumentParser) -> None:
        parser.add_argument('-p', '--profile', default=None,
                            help='AWS configuration profile')
        parser.add_argument('-d', '--dist_dir', default='./dist',
                            help='path to dist dir (default is "./dist")')
        parser.add_argument('-e', '--environment', dest='env_name',
                            help='Name of environment to deploy.')
        parser.add_argument('--env_mapping_file', default='environments.json',
                            type=argparse.FileType('r'),
                            help='path to environments mapping file '
                                 '(default is "environments.json")')

    @classmethod
    def build(cls, conf_name: str):
        """
        Build project
        """
        raise NotImplementedError

    @staticmethod
    def sync_s3(dist_dir: str, s3_path: str, profile: Optional[str]):
        """
        boto3 has no functionality to sync files to S3
        It's much easier to use AWS CLI than implement custom sync using s3fs
        """
        cmd = ['aws', 's3', 'sync', dist_dir, s3_path, '--delete']
        if profile is not None:
            cmd.extend(['--profile', profile])
        subprocess.check_call(cmd, stderr=subprocess.STDOUT)

    @staticmethod
    def invalidate(distribution_id: str, env_name: str, profile: Optional[str]):
        """
        Create CloudFront invalidation
        """
        session = boto3.Session(profile_name=profile)
        cloud_front_client = session.client('cloudfront')
        cloud_front_client.create_invalidation(
            DistributionId=distribution_id,
            InvalidationBatch={
                'Paths': {
                    'Quantity': 1,
                    'Items': ['/*']
                },
                'CallerReference': f'mhs-ci_{time.time()}_{env_name}'
            }
        )

    @classmethod
    def run(cls, dist_dir: str, env_name: str, env_mapping_file: io.TextIOWrapper,
            profile: Optional[str] = None) -> None:

        env_mapping = json.load(env_mapping_file)
        config = env_mapping[env_name]

        conf_name = config['configuration_name']
        s3_path = config['s3_path']
        distribution_id = config['distribution_id']

        cls.build(conf_name)
        cls.sync_s3(dist_dir, s3_path, profile)
        cls.invalidate(distribution_id, env_name, profile)


class DeployFrontend(BaseDeployFrontend):

    @classmethod
    def build(cls, conf_name: str):
        subprocess.check_call(['npm', 'install'], stderr=subprocess.STDOUT)
        subprocess.check_call(['ng', 'build', '--configuration', conf_name], stderr=subprocess.STDOUT)


class DeployFrontendLogin(BaseDeployFrontend):

    @classmethod
    def build(cls, conf_name: str):
        subprocess.check_call(['./build.sh', conf_name], stderr=subprocess.STDOUT)
